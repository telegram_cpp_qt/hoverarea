import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import MyControls 1.0

Window {
    width: 640
    height: 480
    visible: true

    ColumnLayout{
        anchors.centerIn: parent

        Rectangle{

            Layout.preferredWidth: 100
            Layout.preferredHeight: 30
            Layout.margins: 4
            color: '#EEEEEE'
            border.width: hoverArea.containsMouse ? 2 : 0
            border.color: 'red'
            radius: 4
            clip: true

            TextEdit{
                id: textEdit

                anchors.fill: parent
                anchors.margins: 5
                verticalAlignment: Qt.AlignVCenter

                selectByMouse: true
                mouseSelectionMode: TextEdit.SelectCharacters

                HoverAreaQML{
                    id: hoverArea
                    anchors.fill: parent
                }
            }
        }
    }
}

