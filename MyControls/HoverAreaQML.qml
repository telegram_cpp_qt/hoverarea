import QtQuick 2.15

MouseArea {

    acceptedButtons: Qt.NoButton
    scrollGestureEnabled: false
    hoverEnabled: true

    onWheel: function(event){
        event.accepted = false;
    }
}
